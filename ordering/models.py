from django.db import models


class Category(models.Model):
    name = models.CharField(unique=True, max_length=20)
    parent = models.ForeignKey("self", related_name='children', on_delete=models.CASCADE, blank=True, null=True)
    discription = models.TextField(max_length=150)

    def __str__(self):
        return self.name


class Variant(models.Model):
    COLOR_VARIANTS = [
        ("Slug_1", "color_variant_1"),
        ("Slug_2", "color_variant_2"),
        ("Slug_3", "color_variant"),
    ]

    color = models.CharField(choices=COLOR_VARIANTS, default="Slug_1", max_length=30)

    def __str__(self):
        return f"Product variant: {self.color})"


class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.FloatField()
    variant = models.ForeignKey(Variant, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Order(models.Model):
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
