from django.shortcuts import render, redirect
from.models import Product, Order
from.forms import ProductForm, CategoryForm, OrderForm
from .filters import ProductFIlter


def products(request):
    all_products = Product.objects.all()
    product_filter = ProductFIlter(request.GET, queryset=all_products)
    all_products = product_filter.qs
    context = {
        "all_products": all_products,
        "filter": product_filter,
    }
    return render(request, "products.html", context=context)


def create_product(request):
    if request.method == "POST":
        product_form = ProductForm(data=request.POST)
        if product_form.is_valid():
            product_form = product_form.save(commit=False)
            product_form.save()
            return redirect("products")

    else:
        product_form = ProductForm()

    context = {
        "form": product_form,
    }
    return render(request, "create_product.html", context=context)


def get_product_details(request, id):
    product = Product.objects.get(id=id)
    context = {
        "product": product
    }
    return render(request, "product_details.html", context=context)


def update_product(request, id):
    if request.method == "POST":
        product = Product.objects.get(pk=id)
        product_form = ProductForm(instance=product, data=request.POST)
        if product_form.is_valid():
            product_form.save()
            return redirect("products")
    else:
        product = Product.objects.get(pk=id)
        product_form = ProductForm(instance=product)

    return render(request, "update_product.html", {"form": product_form})


def delete_product(request, id):
    Product.objects.filter(id=id).delete()
    return redirect("products")


def delete_order(request, id):
    Order.objects.filter(id=id).delete()
    return redirect("orders")


def create_category(request):
    if request.method == "POST":
        category_form = CategoryForm(data=request.POST)
        if category_form.is_valid():
            category_form = category_form.save(commit=False)
            category_form.save()
            return redirect("products")

    else:
        category_form = CategoryForm()

    context = {
        "form": category_form,
    }
    return render(request, "create_category.html", context=context)


def create_order(request):
    if request.method == "POST":
        order_form = OrderForm(data=request.POST)
        if order_form.is_valid():
            order_form = order_form.save(commit=False)
            order_form.save()
            return redirect("orders")

    else:
        order_form = OrderForm()

    context = {
        "form": order_form,
    }
    return render(request, "create_order.html", context=context)


def orders(request):
    all_orders = Order.objects.all()
    return render(request, "orders.html", {"all_orders": all_orders})
