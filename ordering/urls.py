from django.urls import path
from . import views

urlpatterns = [
    path('products', views.products, name='products'),
    path('products/create', views.create_product, name='create_product'),
    path('products/create_category', views.create_category, name='create_category'),
    path('products/details/<id>', views.get_product_details, name='get_product_details'),
    path('products/edit/<id>', views.update_product, name='update_product'),
    path('products/delete/<id>', views.delete_product, name='delete_product'),
    path('products/ordering', views.create_order, name='create_order'),
    path('products/orders', views.orders, name='orders'),
    path('products/delete_order/<id>', views.delete_order, name='delete_order'),
]
